# Искусственный интеллект и машинное обучение
Выполненные лабораторные работы по курсу: "Искусственный интеллект и машинное обучение"
____

# Лабораторные работы

 № Л/р | Название | Статус| Ссылка
 ----- |----------|-------|------
 1 | Logistic Regression |✅| [Ссылка](https://github.com/nvnovitskiy/artificial-intelligence-and-machine-learning/tree/main/task1)
 2 | Naive Bayes |✅ | [Ссылка](https://github.com/nvnovitskiy/artificial-intelligence-and-machine-learning/tree/main/task2)
 3 | Hypertuning |✅ | [Ссылка](https://github.com/nvnovitskiy/artificial-intelligence-and-machine-learning/tree/main/task3) 
 4 | Future selection |✅ | [Ссылка](https://github.com/nvnovitskiy/artificial-intelligence-and-machine-learning/tree/main/task4) 
 5 | Clustering |✅ | [Ссылка](https://github.com/nvnovitskiy/artificial-intelligence-and-machine-learning/tree/main/task5)
