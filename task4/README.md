# Лабораторная работа №4

## Задание на лабораторную работу:

Найти наиболее информативные признаки для данных вашего варианта. Произвести оценки зависимости точности модели и числа выбранных признаков.

## Результаты, которые необходимо получить в итоге:

1. DataFrame с признаками и их информативностью, отсортированный по убыванию.
2. Графики, показывающие результат оценки зависимости точности модели и числа выбранных признаков.
3. Таблица содержащая f1-score, precision, и recall полученные для лучшей модели, которую вы смогли обучить.
4. Визуализируйте предсказания вашей лучшей модели.

## DataFrame с признаками и их информативностью, отсортированный по убыванию.

![image1](https://github.com/nvnovitskiy/2021-ARTIFICIAL-INTELLEGENCE-AND-MACHINE-LEARNING/blob/main/task4/images/dataframe.png)

## Графики, показывающие результат оценки зависимости точности модели и числа выбранных признаков.

![image2](https://github.com/nvnovitskiy/2021-ARTIFICIAL-INTELLEGENCE-AND-MACHINE-LEARNING/blob/main/task4/images/graphic.png)

## Таблица содержащая f1-score, precision, и recall полученные для лучшей модели, которую вы смогли обучить.

TRAIN:

![image3](https://github.com/nvnovitskiy/2021-ARTIFICIAL-INTELLEGENCE-AND-MACHINE-LEARNING/blob/main/task4/images/class_report2.png)

TEST:

![image4](https://github.com/nvnovitskiy/2021-ARTIFICIAL-INTELLEGENCE-AND-MACHINE-LEARNING/blob/main/task4/images/class_report1.png)

## Визуализируйте предсказания вашей лучшей модели.

![image5](https://github.com/nvnovitskiy/2021-ARTIFICIAL-INTELLEGENCE-AND-MACHINE-LEARNING/blob/main/task4/images/model_visual.png)
